﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YL.Schedule.Lib.Models;

namespace YL.Schedule.Lib.Handlers
{
    public class MemberHandler
    {
        private readonly Schedule_dbContext _dbContext = null;

        public MemberHandler(Schedule_dbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Member> LoginCheckAsync(string id, string pwd)
        {
            var result = await Task.Run(() =>
            {
                var md5 = EasyEncryption.MD5.ComputeMD5Hash(pwd);
                return _dbContext.Member.SingleOrDefault(x => x.Id == id && x.Password == md5);
            });

            return result;
        }

        public async Task<Member> GetMemberAsync(string id)
        {
            var result = await Task.Run(() =>
            {
                return _dbContext.Member.SingleOrDefault(x => x.Id == id);
            });

            return result;
        }

        public async Task<IEnumerable<Member>> GetMembersAsync()
        {
            var result = await Task.Run(() =>
            {
                return _dbContext.Member.ToList();
            });

            return result;
        }

    }
}
