﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YL.Schedule.Lib.Models;

namespace YL.Schedule.Lib.Handlers
{
    public class ScheduleHandler
    {
        private readonly Schedule_dbContext _dbContext = null;

        public ScheduleHandler(Schedule_dbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Item> GetScheduleAsync(int idx)
        {
            var result = await Task.Run(() =>
            {
                var query = _dbContext.Schedule.GroupJoin(_dbContext.Member,
                    schedule => schedule.ManagerId,
                    member => member.Id,
                    (schedule, member) => new
                    {
                        s = schedule,
                        m = member
                    }).SelectMany(obj => obj.m.DefaultIfEmpty(),
                    (x, y) => new Item
                    {
                        Schedule = x.s,
                        Member = y
                    }).SingleOrDefault(x => x.Schedule.Idx == idx);

                //var query = _dbContext.Schedule.Join(_dbContext.Member,
                //    schedule => schedule.ManagerId,
                //    member => member.Id,
                //    (schedule, member) => new Item
                //    {
                //        Schedule = schedule,
                //        Member = member
                //    }
                //    ).SingleOrDefault(x => x.Schedule.Idx == idx);

                return query;
            });

            return result;
        }

        public async Task<IEnumerable<Item>> GetSchedulesAsync(string dept, string date)
        {
            var result = await Task.Run(() =>
            {
                var query = _dbContext.Schedule.GroupJoin(_dbContext.Member,
                    schedule => schedule.ManagerId,
                    member => member.Id,
                    (schedule, member) => new
                    {
                        s = schedule,
                        m = member
                    }).SelectMany(obj => obj.m.DefaultIfEmpty(),
                    (x, y) => new Item
                    {
                        Schedule = x.s,
                        Member = y
                    }).Where(x => x.Schedule.Dept == dept
                        && x.Schedule.VisitDate == DateTime.Parse(date));

                return query;
            });

            return result;
        }

        public async Task<IEnumerable<Item>> GetMySchedulesAsync(string dept, string id, string date)
        {
            var result = await Task.Run(() =>
            {
                var query = _dbContext.Schedule.GroupJoin(_dbContext.Member,
                    schedule => schedule.ManagerId,
                    member => member.Id,
                    (schedule, member) => new
                    {
                        s = schedule,
                        m = member
                    }).SelectMany(obj => obj.m.DefaultIfEmpty(),
                    (x, y) => new Item
                    {
                        Schedule = x.s,
                        Member = y
                    }).Where(x => x.Schedule.Dept == dept
                        && x.Schedule.ManagerId == id
                        && x.Schedule.VisitDate == DateTime.Parse(date));

                //var query = _dbContext.Schedule.Join(_dbContext.Member,
                //    schedule => schedule.ManagerId,
                //    member => member.Id,
                //    (schedule, member) => new Item
                //    {
                //        Schedule = schedule,
                //        Member = member
                //    }
                //    ).Where(x => x.Schedule.ManagerId == id);

                return query;
            });

            return result;
        }

        //public async Task<IEnumerable<Item>> InsertScheduleAsync(Item model)
        //{
        //    var result = await Task.Run(() =>
        //    {
        //        _dbContext.Schedule.Add(model.Schedule);
        //        if (_dbContext.SaveChanges() > 0)
        //        {
        //            var query = _dbContext.Schedule.GroupJoin(_dbContext.Member,
        //                schedule => schedule.ManagerId,
        //                member => member.Id,
        //                (schedule, member) => new
        //                {
        //                    s = schedule,
        //                    m = member
        //                }).SelectMany(obj => obj.m.DefaultIfEmpty(),
        //                (x, y) => new Item
        //                {
        //                    Schedule = x.s,
        //                    Member = y
        //                });

        //            //var query = _dbContext.Schedule.Join(_dbContext.Member,
        //            //    schedule => schedule.ManagerId,
        //            //    member => member.Id,
        //            //    (schedule, member) => new Item
        //            //    {
        //            //        Schedule = schedule,
        //            //        Member = member
        //            //    }
        //            //    ).ToList();

        //            return query;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    });

        //    return result;
        //}

        public async Task<string> InsertScheduleAsync(Item model)
        {
            var result = await Task.Run(() =>
            {
                _dbContext.Schedule.Add(model.Schedule);
                if (_dbContext.SaveChanges() > 0)
                {
                    return "OK";
                }
                else
                {
                    return "FAIL";
                }
            });

            return result;
        }

        //public async Task<Item> UpdateScheduleAsync(Item model)
        //{
        //    var result = await Task.Run(() =>
        //    {
        //        _dbContext.Schedule.Update(model.Schedule);
        //        if (_dbContext.SaveChanges() > 0)
        //        {
        //            var query = _dbContext.Schedule.GroupJoin(_dbContext.Member,
        //                schedule => schedule.ManagerId,
        //                member => member.Id,
        //                (schedule, member) => new
        //                {
        //                    s = schedule,
        //                    m = member
        //                }).SelectMany(obj => obj.m.DefaultIfEmpty(),
        //                (x, y) => new Item
        //                {
        //                    Schedule = x.s,
        //                    Member = y
        //                }).SingleOrDefault(x => x.Schedule.Idx == model.Schedule.Idx);

        //            //var query = _dbContext.Schedule.Join(_dbContext.Member,
        //            //    schedule => schedule.ManagerId,
        //            //    member => member.Id,
        //            //    (schedule, member) => new Item
        //            //    {
        //            //        Schedule = schedule,
        //            //        Member = member
        //            //    }
        //            //    ).SingleOrDefault(x => x.Schedule.Idx == model.Schedule.Idx);

        //            return query;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    });

        //    return result;
        //}

        public async Task<string> UpdateScheduleAsync(Item model)
        {
            var result = await Task.Run(() =>
            {
                _dbContext.Schedule.Update(model.Schedule);
                if (_dbContext.SaveChanges() > 0)
                {
                    return "OK";
                }
                else
                {
                    return "FAIL";
                }
            });

            return result;
        }

        //public async Task<IEnumerable<Item>> DeleteScheduleAsync(int idx)
        //{
        //    var result = await Task.Run(() =>
        //    {
        //        var scheItem = _dbContext.Schedule.SingleOrDefault(x => x.Idx == idx);

        //        _dbContext.Schedule.Remove(scheItem);
        //        if (_dbContext.SaveChanges() > 0)
        //        {
        //            var query = _dbContext.Schedule.GroupJoin(_dbContext.Member,
        //                schedule => schedule.ManagerId,
        //                member => member.Id,
        //                (schedule, member) => new
        //                {
        //                    s = schedule,
        //                    m = member
        //                }).SelectMany(obj => obj.m.DefaultIfEmpty(),
        //                (x, y) => new Item
        //                {
        //                    Schedule = x.s,
        //                    Member = y
        //                });

        //            //var query = _dbContext.Schedule.Join(_dbContext.Member,
        //            //    schedule => schedule.ManagerId,
        //            //    member => member.Id,
        //            //    (schedule, member) => new Item
        //            //    {
        //            //        Schedule = schedule,
        //            //        Member = member
        //            //    }
        //            //    ).ToList();

        //            return query;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    });

        //    return result;
        //}

        public async Task<string> DeleteScheduleAsync(int idx)
        {
            var result = await Task.Run(() =>
            {
                var scheItem = _dbContext.Schedule.SingleOrDefault(x => x.Idx == idx);

                _dbContext.Schedule.Remove(scheItem);
                if (_dbContext.SaveChanges() > 0)
                {
                    return "OK";
                }
                else
                {
                    return "FAIL";
                }
            });

            return result;
        }

    }
}
