﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YL.Schedule.Lib.Models
{
    public partial class Schedule
    {
        [Key]
        public int Idx { get; set; }
        public string Dept { get; set; }
        public string ManagerId { get; set; }
        public DateTime VisitDate { get; set; }
        public string VisitTime { get; set; }
        public string ReVisit { get; set; }
        public string Agency { get; set; }
        public string CusName { get; set; }
        public string CusHP { get; set; }
        public string CusAddr { get; set; }
        public string PredItems { get; set; }
        public string Remark { get; set; }
        public string TokenIdx { get; set; }
        public bool UseBit { get; set; }
        public string InsertId { get; set; }
        public DateTime? InsertDt { get; set; }
        public string UpdateId { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
}
