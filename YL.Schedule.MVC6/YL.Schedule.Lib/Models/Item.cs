﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Schedule.Lib.Models
{
    public class Item
    {
        public Schedule Schedule { get; set; }
        public Member Member { get; set; }
    }
}
