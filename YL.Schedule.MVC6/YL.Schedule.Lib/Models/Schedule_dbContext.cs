﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Schedule.Lib.Models
{
    public class Schedule_dbContext : DbContext
    {
        public Schedule_dbContext()
        {
        }

        public Schedule_dbContext(DbContextOptions<Schedule_dbContext> options) : base(options)
        {
        }

        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=119.196.177.124;Database=YL_Schedule;User Id=ylschedule;Password=ylschedule;");
        }
    }
}
