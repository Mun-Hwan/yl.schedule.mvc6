﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YL.Schedule.Lib.Models
{
    public partial class Member
    {
        [Key]
        [Required]
        public string Id { get; set; }
        [Required]
        public string Password { get; set; }
        public string Name { get; set; }
        public string Dept { get; set; }
        public bool UseBit { get; set; }
        public string InsertId { get; set; }
        public DateTime InsertDt { get; set; }
        public string UpdateId { get; set; }
        public DateTime UpdateDt { get; set; }
    }

    public class LoginUser
    {
        public string Id { get; set; }
        public string Pass { get; set; }
    }
}
