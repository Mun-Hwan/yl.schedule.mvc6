﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using YL.Schedule.Lib.Handlers;
using YL.Schedule.Lib.Models;

namespace YL.Schedule.MVC6.Controllers
{
    [Produces("application/json")]
    [Route("[controller]/[action]")]
    public class ScheduleController : Controller
    {
        private ScheduleHandler _dbHandler = null;

        public ScheduleController(Schedule_dbContext dbcontext)
        {
            _dbHandler = new ScheduleHandler(dbcontext);
        }

        [HttpGet("{idx}")]
        public async Task<IActionResult> Get(int idx)
        {
            var response = new Dictionary<string, object>();

            try
            {
                var r = await _dbHandler.GetScheduleAsync(idx);
                if (r == null)
                {
                    response.Add("result", "FAIL");
                    response.Add("data", "");
                }
                else
                {
                    response.Add("result", "OK");
                    response.Add("data", r);
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
                response.Add("data", "");
            }

            return new ObjectResult(response);
        }

        [HttpGet("{dept}/{id}/{date}")]
        public async Task<IActionResult> GetId(string dept, string id, string date)
        {
            var response = new Dictionary<string, object>();

            try
            {
                var r = await _dbHandler.GetMySchedulesAsync(dept, id, date);
                if (r == null)
                {
                    response.Add("result", "FAIL");
                    response.Add("data", "");
                }
                else
                {
                    response.Add("result", "OK");
                    response.Add("data", r);
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
                response.Add("data", "");
            }

            return new ObjectResult(response);
        }

        [HttpGet("{dept}/{date}")]
        public async Task<IActionResult> GetAll(string dept, string date)
        {
            var response = new Dictionary<string, object>();

            try
            {
                var r = await _dbHandler.GetSchedulesAsync(dept, date);
                if (r == null)
                {
                    response.Add("result", "FAIL");
                    response.Add("data", "");
                }
                else
                {
                    response.Add("result", "OK");
                    response.Add("data", r);
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
                response.Add("data", "");
            }

            return new ObjectResult(response);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody]object content)
        {
            var response = new Dictionary<string, object>();
            var item = JsonConvert.DeserializeObject<Item>(content.ToString());

            try
            {
                if (content != null)
                {
                    var r = await _dbHandler.InsertScheduleAsync(item);
                    response.Add("result", r);
                }
                else
                {
                    response.Add("result", "FAIL");
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
            }

            return new ObjectResult(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]object content)
        {
            var response = new Dictionary<string, object>();
            var item = JsonConvert.DeserializeObject<Item>(content.ToString());

            try
            {
                if (content != null)
                {
                    var r = await _dbHandler.UpdateScheduleAsync(item);
                    response.Add("result", r);
                }
                else
                {
                    response.Add("result", "FAIL");
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
            }

            return new ObjectResult(response);
        }

        [HttpDelete("{idx}")]
        public async Task<IActionResult> Del(int idx)
        {
            var response = new Dictionary<string, object>();

            try
            {
                var r = await _dbHandler.DeleteScheduleAsync(idx);
                response.Add("result", r);
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
            }

            return new ObjectResult(response);
        }

    }
}