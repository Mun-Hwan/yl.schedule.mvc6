﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using YL.Schedule.Lib.Handlers;
using YL.Schedule.Lib.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace YL.Schedule.MVC6.Controllers
{
    [Produces("application/json")]
    //[Route("api/[controller]")]
    [Route("[controller]/[action]")]
    public class MemberController : Controller
    {
        private MemberHandler _dbHandler = null;

        public MemberController(Schedule_dbContext dbcontext)
        {
            _dbHandler = new MemberHandler(dbcontext);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var response = new Dictionary<string, object>();

            try
            {
                //if (!ModelState.IsValid)
                //    throw new ArgumentNullException("아이디가 없습니다.");

                var r = await _dbHandler.GetMemberAsync(id);
                if (r == null)
                {
                    response.Add("result", "FAIL");
                    response.Add("data", "");
                }
                else
                {
                    response.Add("result", "OK");
                    response.Add("data", r);
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
                response.Add("data", "");
            }

            return new ObjectResult(response);
        }

        [HttpPost]
        public async Task<IActionResult> LoginCheck([FromBody]object content)
        {
            var response = new Dictionary<string, object>();

            var loginUser = JsonConvert.DeserializeObject<LoginUser>(content.ToString());

            //Console.WriteLine("Id : " + loginUser.Id);
            //Console.WriteLine("Pass : " + loginUser.Pass);

            try
            {
                //if (!ModelState.IsValid)
                //    throw new ArgumentNullException("아이디나 패스워드가 잘못 되었습니다.");
            
                var r = await _dbHandler.LoginCheckAsync(loginUser.Id, loginUser.Pass);
                if (r == null)
                {
                    response.Add("result", "FAIL");
                    response.Add("data", "");
                }
                else
                {
                    response.Add("result", "OK");
                    response.Add("data", r);
                }
            }
            catch (Exception)
            {
                response.Add("result", "FAIL");
                response.Add("data", "");
            }

            return new ObjectResult(response);
        }
    }
}
